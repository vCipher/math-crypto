﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MathCryptoLibrary
{
    public class Library
    {
        /// <summary>
        /// Умножение по модулю
        /// </summary>
        /// <param name="fstOperand"></param>
        /// <param name="sndOperand"></param>
        /// <param name="mod"></param>
        /// <returns></returns>
        public static int MultipByMod(int fstOperand, int sndOperand, int mod)
        {
            Int64 fstByMod = fstOperand % mod;
            Int64 sndByMod = sndOperand % mod;
            return (int)((fstByMod * sndByMod) % (Int64)mod);
        }

        /// <summary>
        /// Возведения в степень по модулю
        /// </summary>
        /// <param name="num"></param>
        /// <param name="power"></param>
        /// <param name="mod"></param>
        /// <returns></returns>
        public static int PowByMod(int num, int power, int mod)
        {
            int res = num;
            for (int i = 1; i < power; ++i)
            {
                res = MultipByMod(res, num, mod);
            }

            return res;
        }

        /// <summary>
        /// Возвращает является ли число вероятно простым или нет
        /// </summary>
        /// <param name="num"></param>
        /// <param name="attempts"></param>
        /// <returns></returns>
        public static bool FermaTest(int num, int attempts)
        {
            bool isPrimary = true;
            Random rand = new Random();

            for(int i = 0; i < attempts; ++i)
            {
                int randNum = rand.Next(1, num);

                int pow = BinPowByMod(randNum, num - 1, num);
                isPrimary = (pow == 1);
                
                // если число не простое, то заканчиваем тест ферма
                if(!isPrimary) break;
            }

            return isPrimary;
        }

        /// <summary>
        /// Бинарное возведение в степень по модулю
        /// </summary>
        /// <param name="num"></param>
        /// <param name="power"></param>
        /// <param name="mod"></param>
        /// <returns></returns>
        public static int BinPowByMod(int num, int power, int mod)
        {
            if (power == 0)
                return 1;

            if (power % 2 == 1)
            {
                return MultipByMod(BinPowByMod(num, power - 1, mod), num, mod);
            }
            else
            {
                int b = BinPowByMod(num, power / 2, mod);
                return MultipByMod(b, b, mod);
            }
        }

        /// <summary>
        /// Нахождение обратного элемента в поле,
        /// по основанию простого числа
        /// </summary>
        /// <param name="num"></param>
        /// <param name="mod"></param>
        /// <exception cref="MathCryptoLibrary.NoSolutionException">Выбрасывает, когда не существует обратного элемента</exception>
        /// <returns></returns>
        public static int InverseElementByMod(int num, int mod)
        {
            int x, y;
            int d = GCD(num, mod, out x, out y);

            if (d != 1)
                throw new NoSolutionException("Не существует обратного элемента");

            return (x % mod + mod) % mod;
        }

        /// <summary>
        /// Решить сравнение первой степени
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="mod"></param>
        /// <exception cref="MathCryptoLibrary.NoSolutionException">Выбрасывает, когда не существует решения данного сравнения</exception>
        /// <returns></returns>
        public static int ResolveComparison(int a, int b, int mod)
        {
            // сокращаем сравнение
            int commonDiv = GCD(a, b, mod);
            a = a / commonDiv;
            b = b / commonDiv;
            mod = mod / commonDiv;

            int x, y;
            int d = GCD(a, mod, out x, out y);

            if (b % d == 0)
            {
                int res = MultipByMod(x, b, mod);

                // находим наименьший положительный экземпляр класса вычетов
                while (res < 0)
                    res = (res + mod) % mod;

                return res;
            }
            else
                throw new NoSolutionException("Не существует решения данного сравнения");
        }

        /// <summary>
        /// Алгоритм Евклида
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int GCD(int a, int b)
        {
            return (a == 0) ? b : GCD(b % a, a);
        }

        /// <summary>
        /// Алгоритм Евклида
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static int GCD(int a, int b, int c)
        {
            return GCD(GCD(a, b), c);
        }

        /// <summary>
        /// Расширенный алгоритм Евклида
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <returns></returns>
        public static int GCD(int a, int b, out int x, out int y)
        {
            if (a == 0)
            {
                x = 0;
                y = 1;
                return b;
            }

            int x1, y1;
            int d = GCD(b % a, a, out x1, out y1);
            x = y1 - (b / a) * x1;
            y = x1;
            return d;
        }

        /// <summary>
        /// Вычисляет функцию Эйлера
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static int EulerFunction(int num)
        {
            int result = num;
            for (int i = 2; i <= num; ++i)
                if (num % i == 0)
                {
                    while (num % i == 0)
                        num /= i;
                    result -= result / i;
                }
            if (num > 1)
                result -= result / num;
            return result;
        }

        /// <summary>
        /// Раскладывает число в каноническом представлении
        /// </summary>
        /// <param name="num"></param>
        /// <returns></returns>
        public static FactoredNumber Factorize(int num)
        {
            return new FactoredNumber(num);
        }
    }
}
