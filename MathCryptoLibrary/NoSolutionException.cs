﻿using System;

namespace MathCryptoLibrary
{
    public class NoSolutionException : Exception
    {
        public NoSolutionException()
            : base()
        { }

        public NoSolutionException(string message)
            : base(message)
        { }

        public NoSolutionException(string message, Exception innerException)
            : base(message, innerException)
        { }
    }
}
