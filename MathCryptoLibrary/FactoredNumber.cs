﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace MathCryptoLibrary
{
    public class FactoredNumber : IEnumerable<KeyValuePair<int, int>>
    {
        private Dictionary<int, int> _dict;

        public int Number { get; private set; }

        public FactoredNumber(int number)
        {
            this.Number = number;
            this._dict = new Dictionary<int, int>();
            this.Factorize();
        }

        public IEnumerator<KeyValuePair<int, int>> GetEnumerator()
        {
            return this._dict.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this._dict.GetEnumerator();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in this._dict)
                sb.AppendFormat(" {0}^{1} *", item.Key, item.Value);

            return sb.ToString().TrimEnd('*').TrimEnd(' ');
        }

        private void Factorize()
        {
            for (int i = 2; i <= this.Number; ++i)
            {
                if (this.Number % i == 0)
                {
                    int count = 0;
                    for (; this.Number % i == 0; ++count)
                        this.Number /= i;

                    this._dict.Add(i, count);
                }
            }
        }
    }
}
