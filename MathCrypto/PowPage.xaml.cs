﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace MathCrypto
{
    public partial class PowPage : PhoneApplicationPage
    {
        public PowPage()
        {
            InitializeComponent();

            this.Submit.Click += SubmitClick;
        }

        private void SubmitClick(object sender, RoutedEventArgs e)
        {
            try
            {
                int num = int.Parse(this.Base.Text);
                int pow = int.Parse(this.Pow.Text);
                int mod = int.Parse(this.Modul.Text);

                int res = MathCryptoLibrary.Library.PowByMod(num, pow, mod);
                this.Result.Text = res.ToString();
            }
            catch (MathCryptoLibrary.NoSolutionException ex)
            {
                this.Result.Text = ex.Message;
            }
            catch (Exception)
            {
                this.Result.Text = "Ошибка";
            }
        }
    }
}