﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using MathCrypto.Resources;

namespace MathCrypto
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            this.Comparision.Click += ComparisionClick;
            this.EulerFunction.Click += EulerFunctionClick;
            this.EuclidsAlgorithm.Click += EuclidsAlgorithmClick;
            this.InverseElement.Click += InverseElementClick;
            this.Pow.Click += PowClick;
            this.Multiply.Click += MultiplyClick;
            this.Factorisation.Click += FactorisationClick;
            this.FermaTest.Click += FermaTestClick;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void ComparisionClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/ComparasionPage.xaml", UriKind.Relative));
        }

        private void EulerFunctionClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/EulerPage.xaml", UriKind.Relative));
        }

        void EuclidsAlgorithmClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/EuclidPage.xaml", UriKind.Relative));
        }

        void InverseElementClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/InverseElementPage.xaml", UriKind.Relative));
        }

        void PowClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/PowPage.xaml", UriKind.Relative));
        }

        void MultiplyClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/MultiplyPage.xaml", UriKind.Relative));
        }

        void FactorisationClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/FactorisationPage.xaml", UriKind.Relative));
        }

        void FermaTestClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("/FermaTestPage.xaml", UriKind.Relative));
        }

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}