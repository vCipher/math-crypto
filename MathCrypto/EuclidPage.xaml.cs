﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace MathCrypto
{
    public partial class EuclidPage : PhoneApplicationPage
    {
        public EuclidPage()
        {
            InitializeComponent();

            this.Submit.Click += SubmitClick;
        }

        private void SubmitClick(object sender, RoutedEventArgs e)
        {
            try
            {
                int a = int.Parse(this.FirstNum.Text);
                int b = int.Parse(this.SecondNum.Text);

                int x, y;

                int res = MathCryptoLibrary.Library.GCD(a, b, out x, out y);
                this.Result.Text = string.Format("НОД({0}, {1}) = {2}\nx = {3}\ny = {4}", a, b, res, x, y);
            }
            catch (MathCryptoLibrary.NoSolutionException ex)
            {
                this.Result.Text = ex.Message;
            }
            catch (Exception)
            {
                this.Result.Text = "Ошибка";
            }
        }
    }
}