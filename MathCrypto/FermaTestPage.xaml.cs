﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;

namespace MathCrypto
{
    public partial class FermaTestPage : PhoneApplicationPage
    {
        public FermaTestPage()
        {
            InitializeComponent();

            this.Submit.Click += SubmitClick;
        }

        private void SubmitClick(object sender, RoutedEventArgs e)
        {
            try
            {
                int num = int.Parse(this.Num.Text);
                int attempts = int.Parse(this.Attempts.Text);

                if (MathCryptoLibrary.Library.FermaTest(num, attempts))
                {
                    this.Result.Text = "Число вероятно простое";
                }
                else
                {
                    this.Result.Text = "Число составное";
                }
            }
            catch (MathCryptoLibrary.NoSolutionException ex)
            {
                this.Result.Text = ex.Message;
            }
            catch (Exception)
            {
                this.Result.Text = "Ошибка";
            }
        }
    }
}