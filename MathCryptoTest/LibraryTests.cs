﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MathCryptoLibrary;

namespace MathCryptoTest
{
    [TestClass]
    public class LibraryTests
    {
        #region FermaTest
        [TestMethod]
        public void FermaTest_ValidOutput_13()
        {
            int num = 13;
            int attempts = 100;

            Assert.AreEqual(true, Library.FermaTest(num, attempts));
        }

        [TestMethod]
        public void FermaTest_ValidOutput_41()
        {
            int num = 41;
            int attempts = 100;

            Assert.AreEqual(true, Library.FermaTest(num, attempts));
        }

        [TestMethod]
        public void FermaTest_ValidOutput_35317()
        {
            int num = 35317;
            int attempts = 100;

            Assert.AreEqual(true, Library.FermaTest(num, attempts));
        }

        [TestMethod]
        public void FermaTest_ValidOutput_1011001()
        {
            int num = 1011001;
            int attempts = 100;

            Assert.AreEqual(true, Library.FermaTest(num, attempts));
        }

        [TestMethod]
        public void FermaTest_ValidOutput_27644437()
        {
            int num = 27644437;
            int attempts = 100;

            Assert.AreEqual(true, Library.FermaTest(num, attempts));
        }

        [TestMethod]
        public void FermaTest_ValidOutput_1073676287()
        {
            int num = 1073676287;
            int attempts = 100;

            Assert.AreEqual(true, Library.FermaTest(num, attempts));
        }
        #endregion FermaTest

        #region MultipByMod
        [TestMethod]
        public void MultipByMod_11_5_4()
        {
            int fst = 11;
            int snd = 5;
            int mod = 4;

            int expectedRes = (fst * snd) % mod;

            Assert.AreEqual(expectedRes, Library.MultipByMod(fst, snd, mod));
        }

        [TestMethod]
        public void MultipByMod_114_225_53()
        {
            int fst = 114;
            int snd = 225;
            int mod = 53;

            int expectedRes = (fst * snd) % mod;

            Assert.AreEqual(expectedRes, Library.MultipByMod(fst, snd, mod));
        }

        [TestMethod]
        public void MultipByMod_1142_2257_1178()
        {
            int fst = 1142;
            int snd = 2257;
            int mod = 1178;

            int expectedRes = (fst * snd) % mod;

            Assert.AreEqual(expectedRes, Library.MultipByMod(fst, snd, mod));
        }
        #endregion
    }
}
